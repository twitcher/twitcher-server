var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userAchievementSchema = new Schema({
    user:  {
		type: [mongoose.Schema.Types.ObjectId],
		ref: 'User',
		required: true
    },
	achievement: {
		type: [mongoose.Schema.Types.ObjectId],
		ref: 'Achievement',
		required: true
	}
}, {
    timestamps: true
});

var UserAchievements = mongoose.model('UserAchievement', userAchievementSchema);
module.exports = UserAchievements;