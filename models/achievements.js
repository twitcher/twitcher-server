var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var achievementSchema = new Schema({
    name:  {
        type: String,
        required: true
    },
	goal: {
		type: String,
        required: true
	},
    image: {
        type: String,
        required: true
	}
}, {
    timestamps: true
});

var Achievements = mongoose.model('Achievement', achievementSchema);
module.exports = Achievements;