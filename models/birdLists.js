var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var birdListSchema = new Schema({
    location:  {
        type: String,
        required: true
    },
	coordinates: {
		type: [Number],
		index: '2d',
        required: true
	},
    timeframe:  {
        type: String,
        required: true
    },
    from:  {
        type: Date,
        required: true
    },
    to:  {
        type: Date,
        required: true
    },
    user:  {
		type: [mongoose.Schema.Types.ObjectId],
		ref: 'User',
		required: true
    }
}, {
    timestamps: true
});

var BirdLists = mongoose.model('BirdList', birdListSchema);
module.exports = BirdLists;