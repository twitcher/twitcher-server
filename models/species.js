var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var speciesSchema = new Schema({
    name:  {
        type: String,
        required: true
    },
	description: {
		type: [Number],
		index: '2d',
		required: true
	},
    photo:  {
        type: String
    },
    audio:  {
        type: String
    },
	similarSpecies: {
		type: [mongoose.Schema.Types.ObjectId],
		ref: 'Species'
	}
}, {
    timestamps: true
});

var Species = mongoose.model('Species', speciesSchema);
module.exports = Species;