var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var observationSchema = new Schema({
    species:  {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Species',
		required: true
    },
	coordinates: {
		type: [Number],
		index: '2d',
		required: true
	},
    datetime:  {
        type: Date,
		required: true
    },
    notes:  {
        type: String
    },
    media:  {
        type: [String]
    }
}, {
    timestamps: true
});

var Observations = mongoose.model('Observation', observationSchema);
module.exports = Observations;