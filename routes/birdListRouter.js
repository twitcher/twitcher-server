var express = require('express');
var bodyParser = require('body-parser');
var BirdLists = require('../models/birdLists');

var birdListRouter = express.Router();
birdListRouter.use(bodyParser.json());

birdListRouter.route('/')

.get(function(req,res,next){
    BirdLists.find({}, function (err, birdList) {
        if (err) throw err;
        res.json(birdList);
    });
})

.post(function(req, res, next){
	BirdLists.create(req.body, function (err, birdList) {	
		if (err) throw err;
		console.log('Bird List created!');
		var id = birdList._id;		
		
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.end('Added the bird list with id: ' + id);
	});
})

.delete(function(req, res, next){
    BirdLists.remove({}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});

birdListRouter.route('/:birdListId')

.get(function(req,res,next){
    BirdLists.findById(req.params.birdListId, function (err, birdList) {
        if (err) throw err;
        res.json(birdList);
    });
})

.put(function(req, res, next){
    BirdLists.findByIdAndUpdate(req.params.birdListId, {
        $set: req.body
    }, {
        new: true
    }, function (err, birdList) {
        if (err) throw err;
        res.json(birdList);
    });
})

.delete(function(req, res, next){
    BirdLists.findByIdAndRemove(req.params.birdListId, function (err, resp) {        
		if (err) throw err;
        res.json(resp);
    });
});

module.exports = birdListRouter;